import Vue from 'vue'
import VueScrollReveal from 'vue-scroll-reveal'

Vue.use(VueScrollReveal, {
  class: 'v-scroll-reveal',
  duration: 800,
  scale: 1,
  distance: '20px',
  mobile: false,
  viewFactor: 0.1,
  beforeReveal: function (el) {
    el.classList.add('active')
  }
})
