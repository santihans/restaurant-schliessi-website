module.exports = {
  mode: 'universal',

  /*
  ** Headers of the page
  */
  head: {
    title: 'Restaurant Schliessi',
    titleTemplate: '%s - Restaurant Schliessi',
    link: [
      {
        rel: 'stylesheet',
        type: 'text/css',
        href: 'https://fonts.googleapis.com/css?family=Noto+Sans|Noto+Serif'
      }
    ]
  },

  meta: {
    name: 'Restaurant Schliessi',
    description: 'Schliessi, Restaurant, Buvette, Musik, Partys',
    theme_color: '#212121',
    ogHost: 'https://www.restaurant-schliessi.ch',
    ogImage: { path: '/og-image.png' },
    twitterCard: 'summary',
    twitterCreator: '@santihans4056'
  },

  manifest: {
    name: 'Restaurant Schliessi',
    short_name: 'Schliessi',
    description: 'Schliessi, Restaurant, Buvette, Musik, Partys'
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#819529' },

  /*
  ** Global CSS
  */
  css: [],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    {
      src: '~/plugins/vueScrollReveal',
      ssr: false
    }
  ],

  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://github.com/nuxt-community/axios-module#usage
    '@nuxtjs/axios',
    '@nuxtjs/pwa'
  ],
  /*
  ** Axios module configuration
  */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
  },

  /*
  ** Build configuration
  */
  build: {
    extractCSS: true,
    /*
    ** You can extend webpack config here
    */
    extend (config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}
