importScripts('/_nuxt/workbox.3de3418b.js')

const workboxSW = new self.WorkboxSW({
  "cacheId": "restaurant-schliessi-website",
  "clientsClaim": true,
  "directoryIndex": "/"
})

workboxSW.precache([
  {
    "url": "/_nuxt/140da203de2f9ecfee02.js",
    "revision": "36df35bb064f1290b62da940bc2523fb"
  },
  {
    "url": "/_nuxt/51e6f1e8e6875e282019.js",
    "revision": "9866297b0887f5e3c369dbd1d15144f5"
  },
  {
    "url": "/_nuxt/9de25e60ceb38c786072.js",
    "revision": "d26a149345756493ee046426e484db4a"
  }
])


workboxSW.router.registerRoute(new RegExp('/_nuxt/.*'), workboxSW.strategies.cacheFirst({}), 'GET')

workboxSW.router.registerRoute(new RegExp('/.*'), workboxSW.strategies.networkFirst({}), 'GET')

