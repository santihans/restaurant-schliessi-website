const main = [
  {
    path: '/',
    label: 'Startseite'
  },
  {
    path: '/ueber-uns',
    label: 'Über uns'
  },
  {
    path: '/impressionen',
    label: 'Impressionen'
  },
  {
    path: '/kontakt',
    label: 'Kontakt'
  }
]

const about = [
  {
    path: '/presse',
    label: 'Presse'
  },
  {
    path: '/links',
    label: 'Links'
  },
  {
    path: '/offene-stellen',
    label: 'Offene Stellen'
  }
]

const social = [
  {
    url: 'https://www.facebook.com/RestaurantBuvetteSchliessi/',
    label: 'Facebook'
  }
]

export { main, about, social }
